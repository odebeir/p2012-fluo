import wx

from traits.api import HasTraits,Any, Instance, Button
from mpl_figure_editor import MPLFigureEditor
from traitsui.api import View, Item, HSplit, VGroup

import numpy as np
from numpy import fft
import matplotlib.pyplot as plt
from matplotlib.figure import Figure

class FourierInteractive(HasTraits):

    source_fig = Instance(Figure, ())
    dest_fig = Instance(Figure, ())

    transform_but = Button('FFT')

    view = View(HSplit(
                    HSplit(
                        Item('source_fig', editor=MPLFigureEditor(), show_label=False),
                        Item('dest_fig', editor=MPLFigureEditor(), show_label=False),
                    ),
                    VGroup(
                        Item('transform_but', show_label=False),
                        Item('transform_but', show_label=False),
                        Item('transform_but', show_label=False),
                    ),
                ),
        width=400,
        height=300,
        resizable=True)

    def __init__(self,ima):
        super(FourierInteractive, self).__init__()
        self.ima = ima
        self.source_axes = self.source_fig.add_subplot(111)
        self.source_axes.imshow(self.ima,interpolation='nearest')
        self.dest_axes = self.dest_fig.add_subplot(111)

    def _transform_but_fired(self):
        F = fft.fftshift(np.abs(fft.fft2(self.ima)))
        self.dest_axes.imshow(F,interpolation='nearest')
        wx.CallAfter(self.dest_fig.canvas.draw)


if __name__ == "__main__":
    # Create a window to demo the editor

    ima = plt.imread('../../test/data/cameraman.tif')


    app = FourierInteractive(ima)
    app.configure_traits()