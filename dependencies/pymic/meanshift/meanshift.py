# -*- coding: utf-8 -*-
'''
Test the scipy weave module to the meanshift function (computed on a triangle and in a near future, using integral image
use Weave for C compilation
'''
__author__ = 'olivier'

#Stuff related to the inline compilation required to return a value with weave.inline
import sys, os

import pylab

import numpy as npy
from scipy.weave import inline
import scipy

from scipy.ndimage import measurements

def dtype2ctype(array):
    """convert numpy type in C equivalent type
    """
    types = {
        npy.dtype(npy.float64): 'double',
        npy.dtype(npy.float32): 'float',
        npy.dtype(npy.int32):   'int',
        npy.dtype(npy.uint32):  'unsigned int',
        npy.dtype(npy.int16):   'short',
        npy.dtype(npy.uint8):   'unsigned char',
        npy.dtype(npy.uint16):  'unsigned short',
    }
    return types.get(array.dtype)

def LUT(type,exp=1):
    """Returns a 256 wide numpy array LUT exp allo to enhance the character of the lut
    """
    if type is 'white':
        return (npy.arange(256.0,dtype = 'float64'))**exp
    if type is 'black':
        return (255.0-(npy.arange(256.0,dtype = 'float64')))**exp
                   
def meanshift(ima,triangleList,offset_x,offset_y,lut=None,verbose = False):
    """compute the meanshift for each triangle in the triangleList

    :param ima: image array
    :type ima: uint8
    :param lut: lookup table applied to each ima pixel
    :type lut: float64 table of lookup (8bit=256 values)
    :param triangleList: list of the triangle to process
    :type triangleList:
    :param offset_x: constant value added to triangle coordinates
    :type offset_x: float
    :param offset_y: constant value added to triangle coordinates
    :type offset_y: float
    :param verbose: True : displays details during C code executes (default is False)
    :type verbose: Bool
    :returns:   centroid for each triangle and several statistics such as area, sum,...

    |            DATA[0] = (double)sumXw;//centroid
    |            DATA[1] = (double)sumYw;//centroid
    |            DATA[2] = (double)surf;//surfCrisp
    |            DATA[3] = (double)surfalpha;//surfAlpha
    |            DATA[4] = (double)totalvalue;//sum
    |            DATA[5] = (double)gmax; //max
    |            DATA[6] = (double)gmin; //min
    |            if(surfalpha>0.0):
    |                DATA[7] = (double)(totalvalue/surfalpha);//mean
    |            else:
    |                DATA[7] = (double)0.0;

    :raises: TypeError
    """

    if not isinstance(ima,npy.ndarray):
        raise TypeError('2D numpy.array expected')
    if not (ima.dtype == npy.uint8) :
        raise TypeError('uint8 numpy.array expected')
    if not(len(ima.shape) == 2):
        raise TypeError('2D numpy.array expected')
    
        
    #activate or de-activate verbose in C code
    if verbose:
        VERBOSE = '#define VERBOSE True'
    else:
        VERBOSE = '#undef VERBOSE'
        
    #code contains the main watershed C code
    code = \
"""
{verbose}
unsigned char *IN       = (unsigned char *) PyArray_GETPTR1(ima_array,0);
double *OUT      = (double *) PyArray_GETPTR1(out_array,0);
double *LUT      = (double *) PyArray_GETPTR1(lut_array,0);
double *TRIANGLE = (double *) PyArray_GETPTR1(triangle_array,0);

#ifdef  VERBOSE    
    printf("(meanshift.c)\\n");
#endif

int N_IN = PyArray_SIZE(ima_array);
int m_IN = PyArray_DIM(ima_array,0); 
int n_IN = PyArray_DIM(ima_array,1);
int sizex = n_IN;
int sizey = m_IN;  
int n;  
double off_x = offset_x;
double off_y = offset_y;

// call the function defined in the meanshift.c file

//return an int to Python
n = compute_g(IN,sizex,sizey,off_x,off_y,TRIANGLE,OUT,LUT);
""".format(verbose = VERBOSE)

    extra_code = open(os.path.join(os.path.split(__file__)[0],'meanshift.c')).read()
    
    if lut is None:
        lut = npy.arange(256,dtype = 'float64')
    
    n = len(triangleList)
    shift = npy.zeros((n,8),dtype = 'float64', order='C')
    out = npy.zeros(8,dtype = 'float64', order='C')
    for i,triangle in enumerate(triangleList):
        out[:] = 0.0
        inline(code, ['ima','out','triangle','lut','offset_x','offset_y'],support_code=extra_code)
        shift[i,:] = npy.copy(out)
    return shift


    
def modelCell(x,y,N,R):
    """Returns an ensemble of triangles centered on xy
    """
    triangleList = []
    p0 = npy.array((x,y))
    #internal pies
    for i in range(N):
        p1 = p0 +(R*npy.cos(i*2*npy.pi/N),R*npy.sin(i*2*npy.pi/N))
        p2 = p0 +(R*npy.cos((i+1)*2*npy.pi/N),R*npy.sin((i+1)*2*npy.pi/N))
        tri = npy.array((p0[0],p0[1],p1[0],p1[1],p2[0],p2[1]))              
        triangleList.append(tri)
    return triangleList

def modelCellInvert(x,y,N,R):
    """Returns an ensemble of triangles centered on xy but with large base at the center
    """
    triangleList = []
    center = npy.array((x,y))
    #internal pies
    angle = 2*npy.pi/N
    for i in range(N):
        p0 = center-(R*npy.cos((i+.5)*angle),R*npy.sin((i+.5)*angle))
        p1 = p0+(R*npy.cos(i*angle),R*npy.sin(i*angle))
        p2 = p0+(R*npy.cos((i+1)*angle),R*npy.sin((i+1)*angle))
        tri = npy.array((p0[0],p0[1],p1[0],p1[1],p2[0],p2[1]))
        triangleList.append(tri)
    return triangleList  
      
def testMeanshift():
    """open a binarised test image, compute meanshift for some triangle
    """
    print 'test MS'
    im = scipy.misc.imread('../../test/data/exp0001.jpg')
    cellLocations = [(387,143),(298,290),(386,323),(415,360),(281,394)]
    N = 16
    RWhite = 30
    RBlack = 15
    triangleListBlack = []
    triangleListWhite = []
    for x,y in cellLocations:
        tWhite = modelCell(x,y,N,RWhite)
        tBlack = modelCellInvert(x,y,N,RBlack)
        triangleListWhite.extend(tWhite)
        triangleListBlack.extend(tBlack)
    
    shift_white = meanshift(im,triangleListWhite,0.0,0.0,lut = LUT('white',10))
    shift_black = meanshift(im,triangleListBlack,0.0,0.0,lut = LUT('black',10))
    
    
    
    fig = pylab.figure(1)
    ax = fig.add_subplot(111)

    pylab.imshow(im, interpolation='nearest')
    
    
    #draw triangle
    idxx = [0,2,4,0] 
    idxy = [1,3,5,1]
    for tri in triangleListBlack:        
        pylab.plot(npy.take(tri,idxx),npy.take(tri,idxy),color = [.5,.5,.5])
    for tri in triangleListWhite:
        pylab.plot(tri[0:6:2],tri[1:6:2],color = [.5,.5,.5])
        
    #draw centroids
    for sh in shift_white:
        r = pylab.Rectangle((sh[0]-1,sh[1]-1),3,3, facecolor=[.8,.8,.8])
        ax.add_artist(r)
    for sh in shift_black:
        r = pylab.Rectangle((sh[0]-1,sh[1]-1),3,3, facecolor=[.1,.1,.1])
        ax.add_artist(r)

            
    pylab.show()

def testMeanshift1():
    """open a binarised test image, compute meanshift for some triangle, NO DISPLAY
    """
    print 'test MS'
    im = scipy.misc.imread('../../test/data/exp0001.jpg')
    cellLocations = [(387,143),(298,290),(386,323),(415,360),(281,394)]
    N = 16
    RWhite = 30
    RBlack = 15
    triangleListBlack = []
    triangleListWhite = []
    for x,y in cellLocations:
        tWhite = modelCell(x,y,N,RWhite)
        tBlack = modelCellInvert(x,y,N,RBlack)
        triangleListWhite.extend(tWhite)
        triangleListBlack.extend(tBlack)
    
    shift_white = meanshift(im,triangleListWhite,0.0,0.0,lut = LUT('white',10))
    shift_black = meanshift(im,triangleListBlack,0.0,0.0,lut = LUT('black',10))
    
    print shift_white
    print shift_black

if __name__ == "__main__":
    import doctest
    doctest.testmod()
    import cProfile
    cProfile.run('testMeanshift1()','../../test/temp/fooprof')
    import pstats
    p = pstats.Stats('../../test/temp/fooprof')
    p.strip_dirs().sort_stats('cumulative').print_stats()
    testMeanshift()
#    help(meanshift)
    