'''
Created on Nov 11, 2010

test zviread python module

@author: olivier
'''
import struct
import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
from scipy.ndimage import measurements

import os
import sys
sys.path.append('../')
from zvi import zviread

import os




def display_all():
    
#    dir = ['../../test/data/'+f for f in os.listdir('../../test/data/') if 'zvi' in f]
    path = '/media/data/zeiss_zvi/Laure_tpima'
#    path = '../../test/data'
    dir = [os.path.join(path,f) for f in os.listdir(path) if 'zvi' in f]
    print dir
    color = [cm.gray,cm.Blues,cm.Greens,cm.Oranges]
    
    for i,fname in enumerate(dir):
        print fname
        plt.figure(i)
        for p in range(4):
            try:
                b = zviread(fname,p).Image.Array
                plt.subplot(2,2,p+1)
                plt.imshow(b,origin='lower',cmap=color[p],interpolation='nearest')
                plt.colorbar()
            except :
                print 'Plane %d unavailable'%p
        plt.title(fname)

    plt.show()



if __name__ == "__main__":
    display_all()
