import numpy as npy

def heaviside(z):
    """Heaviside step function (smoothed version)"""
    epsilon = 1e-1
    H = npy.zeros(z.shape)
    idx1 = z>epsilon
    idx2 = npy.logical_and(z<epsilon,z>-epsilon)
    ze = z[idx2]/epsilon

    H[idx1] = 1
    H[idx2] = .5 *(1 + ze + 1.0/npy.pi * npy.sin(npy.pi*ze))


    return H

def main():
    import matplotlib.pyplot as plt

    z = npy.linspace(-1,1,1000)
    h = heaviside(z)
    plt.plot(h)
    plt.show()

if __name__ == '__main__':
    main()

