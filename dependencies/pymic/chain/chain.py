# -*- coding: utf-8 -*-
'''
Test the scipy weave module to the chain function
use Weave for C compilation
'''

#Stuff related to the inline compilation required to return a value with weave.inline
import sys, os

import pylab
import numpy as npy
from scipy.weave import inline
import scipy

from scipy.ndimage import measurements

def dtype2ctype(array):
    """convert numpy type in C equivalent type
    """
    types = {
        npy.dtype(npy.float64): 'double',
        npy.dtype(npy.float32): 'float',
        npy.dtype(npy.int32):   'int',
        npy.dtype(npy.uint32):  'unsigned int',
        npy.dtype(npy.int16):   'short',
        npy.dtype(npy.uint8):   'unsigned char',
        npy.dtype(npy.uint16):  'unsigned short',
    }
    return types.get(array.dtype)

watershedMode = ['unmarked','marked','modifiedGradient']
                   
def chain(ima,labels = None,ext=0,verbose = False):
    """compute the chain of the label image im

    :param ima: image array
    :type ima: uint32 numpy.ndarray
    :param labels: list of the labels to process, if None, all the present labels are processed
    :type labels: list
    :param ext: if 1 external border is computed
    :type ext: int[0-1]
    :param verbose: True : displays details during C code executes (default is False)
    :type verbose: Bool

    :returns:  chain for each label
    :raises: TypeError

    example:

    >>> im = scipy.misc.imread('../../test/data/blocs.png')
    >>> m = measurements.label(im)
    >>> label = m[0]
    >>> contours = chain(label,labels = None,ext = 0)
    """

    if not isinstance(ima,npy.ndarray):
        raise TypeError('2D numpy.array expected')
    if not (ima.dtype == npy.int32) :
        raise TypeError('int32 numpy.array expected')
    if not(len(ima.shape) == 2):
        raise TypeError('2D numpy.array expected')
    if labels is None:
        labels = range(1,npy.max(ima)+1)
    
    #buffer limit
    CHAINMAXLENGTH = 100000
    chainX   = npy.zeros(CHAINMAXLENGTH,dtype = 'int32', order='C')
    chainY   = npy.zeros(CHAINMAXLENGTH,dtype = 'int32', order='C')
    N   = npy.zeros(1,dtype = 'int32', order='C')
        
    #activate or de-activate verbose in C code
    if verbose:
        VERBOSE = '#define VERBOSE True'
    else:
        VERBOSE = '#undef VERBOSE'
        
    #code contains the main watershed C code
    code = \
"""
%s
#define CHAINMAXLENGTH %d 
int *IN      = (int *) PyArray_GETPTR1(ima_array,0);
int *CHAINX      = (int *) PyArray_GETPTR1(chainX_array,0);
int *CHAINY      = (int *) PyArray_GETPTR1(chainY_array,0);
int *n      = (int *) PyArray_GETPTR1(N_array,0);

#ifdef  VERBOSE    
    printf("(chain.c)\\n");
#endif

int N_IN = PyArray_SIZE(ima_array);
int m_IN = PyArray_DIM(ima_array,0); 
int n_IN = PyArray_DIM(ima_array,1);
int sizex = n_IN;
int sizey = m_IN;  

// call the function defined in the chain.c file

//return an int to Python
if(ext)
    *n = build_chain4ex(IN,sizex,sizey,label,CHAINX,CHAINY);
else
    *n = build_chain(IN,sizex,sizey,label,CHAINX,CHAINY);

"""% (VERBOSE,CHAINMAXLENGTH)

#    print code

    contours = []
    
    extra_code = '#define CHAINMAXLENGTH %d '%(CHAINMAXLENGTH) + open(os.path.join(os.path.split(__file__)[0],'chain.c')).read()
    for label in labels:
        inline(code, ['ima','chainX','chainY','label','ext','N'],support_code=extra_code)
        n = N[0]
        contours.append((label,n,npy.copy(chainX[0:n]),npy.copy(chainY[0:n])))
    return contours
    
def testChain():
    """open a binarised test image, compute internal and external chain and display it in overlay
    """
    im = scipy.misc.imread('../../test/data/blocs.png')
    m = measurements.label(im)
    label = m[0]
    print label
    scipy.misc.imsave('../../test/temp/chainLabel.tif',label)
    
    contours = chain(label,labels = None,ext = 0)
    contoursExt = chain(label,labels = None,ext = 1)
    
    
    pylab.figure(1)
    pylab.imshow(label, interpolation='nearest')
    
    for c in contours:
        print c
        
        pylab.plot(c[2]-1,c[3]-1)
        x0 = c[2][0]
        y0 = c[3][0]
        xg = c[2].mean()
        yg = c[3].mean()
        pylab.text(x0,y0,'label%d'%c[0],color=[.5,.5,.5])
        print len(c[2])
        
#    for c in contoursExt:
#        pylab.plot(c[2]-1,c[3]-1)
        
    pylab.show()


if __name__ == "__main__":
    import doctest
    doctest.testmod()

    testChain()
#    help(chain)
    