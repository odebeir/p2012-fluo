import sys
sys.path.append('../dependencies')

import os
import matplotlib.pyplot as plt

from pymic.zvi import getcount,zviread

data_path = '../data/fish'

fnames = os.listdir(data_path)

for filename in fnames:
    # read ZVI file
    file_path = os.path.join(data_path,filename)
    n_layers = getcount(file_path)
    print data_path
    print filename
    print 'number of planes : ', n_layers
    layers = []
    for i in range(n_layers):
        data = zviread(file_path,i)[-1][-1]
        layers.append(data)
    print '*'*80

    # display layers
    for data in layers:
        print data
        plt.figure()
        plt.imshow(data)
    plt.show()

